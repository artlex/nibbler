/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SDL_ANIM_class.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 14:53:32 by bcherkas          #+#    #+#             */
/*   Updated: 2019/03/26 15:17:54 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SDL_ANIM_CLASS_H
# define SDL_ANIM_CLASS_H

# include <vector>
# include <memory>

class SDL_IMAGE;

class SDL_ANIM {
public:
	using imageptr = std::shared_ptr<SDL_IMAGE>;
	using imagevector = std::vector<imageptr>;

	SDL_ANIM(imagevector &&images, unsigned int timestep, unsigned int maxtime);
	SDL_ANIM() = delete;
	SDL_ANIM(SDL_ANIM const &obj);
	SDL_ANIM(SDL_ANIM &&obj);
	~SDL_ANIM();

	SDL_ANIM	&operator=(SDL_ANIM const &o) = delete;

	void		changeAnimationTick(unsigned int ticks);
	bool		processAnimation(void);
	bool		putAnimation(int x, int y, int sizex, int sizey, double angle);

private:
	const imagevector	_imgvector;
	unsigned int		_cycles_step;
	unsigned int		_lifetime_cycles;
	unsigned int		_size;
	unsigned int		_cycles = 0;
	unsigned int		_iter = 0;
};

#endif
