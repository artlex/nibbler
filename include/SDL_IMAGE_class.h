/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SDL_IMAGE_class.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 14:53:32 by bcherkas          #+#    #+#             */
/*   Updated: 2019/03/12 18:24:10 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SDL_IMAGE_CLASS_H
# define SDL_IMAGE_CLASS_H

# include "SDL2/SDL.h"
# include <memory>

class SDL_API;

class SDL_IMAGE {
private:
	using	surfaceptr = std::unique_ptr<SDL_Surface, void (*)(SDL_Surface *)>;
	using	textureptr = std::unique_ptr<SDL_Texture, void (*)(SDL_Texture *)>;

public:

	SDL_IMAGE() = delete;
	SDL_IMAGE(SDL_API *apiptr, int width, int height);
	SDL_IMAGE(SDL_API *apiptr, std::string const &str);
	SDL_IMAGE(SDL_API *apiptr, std::string const &str, unsigned int colorkey);
	SDL_IMAGE(SDL_API *apiptr, SDL_Surface *surface);
	SDL_IMAGE(SDL_IMAGE const &obj) = delete;
	SDL_IMAGE(SDL_IMAGE &&obj);
	~SDL_IMAGE();

	SDL_IMAGE	&operator=(SDL_IMAGE const &o) = delete;

	bool		fillImage(unsigned int r, unsigned int g, unsigned int b);
	bool		putImage(int x, int y, int sizex, int sizey);
	bool		putImageRotate(int x, int y, int sizex, int sizey, double rotate);

/*
** Getters/Setter
*/

	SDL_Surface	*getSurface();
	SDL_Texture	*getTexture();
	int			getWidth() const;
	int			getHeight() const;
	void		getSize(int &width, int &height) const;

private:
	SDL_API		*_apiptr;
	surfaceptr	_surface;
	textureptr	_texture;
	int			_width;
	int			_height;
};

#endif
