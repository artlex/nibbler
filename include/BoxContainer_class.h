/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   BoxContainer.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/17 15:45:02 by bcherkas          #+#    #+#             */
/*   Updated: 2019/03/17 17:48:26 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BOXCONTAINER_CLASS_H
# define BOXCONTAINER_CLASS_H

# include <list>
# include "Box_class.h"

class BoxContainer {

private:
	using	boxlist = std::list<const Box>;

	static BoxContainer	*_ptr;

public:

	static BoxContainer	*getContainer();
	static void			destroyContainer();

	BoxContainer(BoxContainer const &) = delete;
	~BoxContainer();
	BoxContainer	&operator=(BoxContainer const &) = delete;

	Box const		*createBox(int x, int y, int width, int height);
	Box const		*findFirstIntersection(int x, int y);
	bool			exist(Box const *box);
	bool			deleteBox(Box const *box);

private:
	BoxContainer() = default;

	boxlist		_list;
};

#endif
