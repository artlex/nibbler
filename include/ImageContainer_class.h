/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ImageContainer_class.h                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/06 18:37:06 by bcherkas          #+#    #+#             */
/*   Updated: 2019/03/12 14:31:27 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IMAGECONTAINER_CLASS_H
# define IMAGECONTAINER_CLASS_H

# include "SDL_IMAGE_class.h"
# include <memory>
# include <map>

class ImageContainer {
public:
	using imageptr = std::shared_ptr<SDL_IMAGE>;
	using imagemap = std::map<int, imageptr>;

	static ImageContainer	*_ptr;
	static ImageContainer	*getContainer();
	static void				destroyContainer();

	ImageContainer(ImageContainer const &) = delete;
	~ImageContainer();

	ImageContainer	&operator=(ImageContainer const &) = delete;
	
/*
** Getters/Setters
*/

	int			addImage(SDL_IMAGE &&image);
	bool		putImage(int number, int xpos, int ypos, int xsize, int ysize);
	bool		putImageRotate(int number, int xpos, int ypos, int xsize, int ysize, double angle);
	imageptr	getImage(int index);
	bool		delImage(int index);

private:

	ImageContainer() = default;

	imagemap		_imagemap;
	int				_imagemap_maxelement = 0;
};

#endif
