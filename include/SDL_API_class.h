/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SDL_API_class.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/06 18:37:06 by bcherkas          #+#    #+#             */
/*   Updated: 2019/05/02 17:11:59 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SDL_API_CLASS_H
# define SDL_API_CLASS_H

# include "SDL2/SDL.h"
# include <iostream>
# include <map>
# include "SDL_IMAGE_class.h"

class SDL_API {
public:
	static SDL_API		*getAPI(int width = 800, int height = 800);
	static void			destroyAPI();
	static std::string	getImgError(std::string const &str);
	static std::string	getError(std::string const &str);
	static void			printError(char const *str);

private:

	static SDL_API		*_ptr;

	using winptr = std::unique_ptr<SDL_Window, void (*)(SDL_Window *)>;
	using rendptr = std::unique_ptr<SDL_Renderer, void (*)(SDL_Renderer *)>;
	using imagemap = std::map<int, SDL_IMAGE>;
	using movehandler = void (*)(int);

public:

	SDL_API() = delete;
	SDL_API(SDL_API const &) = delete;
	~SDL_API();

	SDL_API	&operator=(SDL_API const &) = delete;
	
	bool	createRenderer(int window_flags, int render_flags);

	void	update();
	bool	checkEvents();
	bool	clearScreen();

/*
** Getters/Setters
*/
	SDL_Renderer	*getRenderer();
	SDL_Window		*getWindow();

	int		getWindowWidth() const;
	int		getWindowHeight() const;
	void	getWindowSize(int &width, int &height) const;
	bool	getMouseState(int *x, int *y) const;

	unsigned int	getTime() const;

	void	setMoveHandler(movehandler handle);

private:
	SDL_API(size_t width, size_t height);
	void			buttonEvent(SDL_Event &event);

	size_t			_width;
	size_t			_height;
	winptr			_window;
	rendptr			_renderer;
	movehandler		_movehandler = nullptr;
	bool			_hasMoveHandler = false;
};

#endif
