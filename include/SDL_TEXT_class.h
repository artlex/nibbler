/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SDL_TEXT_class.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/12 16:10:38 by bcherkas          #+#    #+#             */
/*   Updated: 2019/03/12 18:38:31 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SDL_TEXT_CLASS_H
# define SDL_TEXT_CLASS_H

# include <memory>
# include "SDL2/SDL_ttf.h"
# include <string>
# include <map>

class SDL_API;

class SDL_IMAGE;

class SDL_TEXT {
	public:
		static SDL_TEXT	*getAPI(SDL_API *apiptr = nullptr);
		static void		destroyAPI();
	private:
		using fontptr = std::unique_ptr<TTF_Font, void (*)(TTF_Font *)>;
		using fontmap = std::map<int, fontptr>;
		using allmap = std::map<const std::string, fontmap>;

		static SDL_TEXT	*_ptr;
	public:
		SDL_TEXT() = delete;
		SDL_TEXT(SDL_TEXT const &) = delete;
		~SDL_TEXT();

		SDL_TEXT	&operator=(SDL_TEXT const &) = delete;

		SDL_IMAGE	renderText(std::string const font, std::string const text, unsigned int color, int textsize);

	private:
		SDL_TEXT(SDL_API *apiptr);
		TTF_Font	*fontManip(std::string const &font, int ptsize);

		SDL_API		*_apiptr;
		allmap		_allmap;
};

#endif
