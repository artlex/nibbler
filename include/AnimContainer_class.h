/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AnimContainer_class.h                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/06 18:37:06 by bcherkas          #+#    #+#             */
/*   Updated: 2019/03/26 15:52:05 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ANIMCONTAINER_CLASS_H
# define ANIMCONTAINER_CLASS_H

# include "SDL_ANIM_class.h"
# include <map>

class AnimContainer {
private:
	using imagemap = std::map<int, SDL_ANIM>;

	class		AnimPos {
		public:
			AnimPos(int xpos, int ypos, int xsize, int ysize, SDL_ANIM const &anim);
			AnimPos(AnimPos const &);
			~AnimPos();

			int			_xpos;
			int			_ypos;
			int			_xsize;
			int			_ysize;
			SDL_ANIM	_anim;
			double		_angle;
			bool		_pause;
			bool		_hide;
	};

	using activemap = std::map<int, AnimPos>;

public:

	static AnimContainer	*_ptr;
	static AnimContainer	*getContainer();
	static void				destroyContainer();

	AnimContainer(AnimContainer const &) = delete;
	~AnimContainer();

	AnimContainer	&operator=(AnimContainer const &) = delete;
	
/*
** Getters/Setters
*/

	int		addAnimation(SDL_ANIM &image);
	int		addAnimation(SDL_ANIM &&image);
	int		putAnimation(int index, int x, int y, int xsize, int ysize);

	bool	moveActive(int index, int x, int y);
	bool	rotateActive(int index, double angle);
	void	drawActive();
	bool	pauseActive(int index, bool pause);
	bool	hideActive(int index, bool pause);
	bool	shiftActive(int index, unsigned int ticks);

	bool	delAnimation(int index);
	bool	delActive(int index);

private:

	AnimContainer() = default;

	imagemap		_imagemap;
	activemap		_activeAnimations;
	int				_imagemap_maxelement = 0;
	int				_activemap_maxelement = 0;
};



#endif
