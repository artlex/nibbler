/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Box_class.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/17 16:16:20 by bcherkas          #+#    #+#             */
/*   Updated: 2019/03/26 19:29:40 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BOX_CLASS_H
# define BOX_CLASS_H

#include <set>

class Box {
public:
	Box();
	Box(int x, int y, int w, int h);
	Box(Box const &);
	~Box();
	Box	&operator=(Box const &);

	bool	intersect(int x, int y) const;

	void	addAnimation(int fd) const;

	int		posx;
	int		posy;
	int		width;
	int		height;
private:
	mutable std::set<int>	_images;
	mutable std::set<int>	_animations;
};

#endif
