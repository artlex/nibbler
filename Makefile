.PHONY: all clean fclean re

CC=clang++
CFLAGS=-Wall -Wextra -Werror -std=c++14 -Iinclude

SDLLIB= -L$(HOME)/.brew/opt/sdl2/lib/ -lSDL2 \
		-L$(HOME)/.brew/opt/sdl2_image/lib/ -lSDL2_image \
		-L$(HOME)/.brew/opt/sdl2_ttf/lib/ -lSDL2_ttf \
		-framework OpenGL
SDLINC=-I$(HOME)/.brew/opt/sdl2/include \
	   -I$(HOME)/.brew/opt/sdl2_image/include \
	   -I$(HOME)/.brew/opt/sdl2_ttf/include \
	   -I$(HOME)/.brew/opt/sdl2/include/SDL2

SRC=SDL_API_class.cpp SDL_IMAGE_class.cpp ImageContainer_class.cpp SDL_ANIM_class.cpp \
		AnimContainer_class.cpp SDL_TEXT_class.cpp Box_class.cpp BoxContainer_class.cpp \
		shared_api.cpp

OBJ=$(SRC:.cpp=.o)
INC=SDL_API_class.h SDL_IMAGE_class.h ImageContainer_class.h SDL_ANIM_class.h \
		AnimContainer_class.h SDL_TEXT_class.h Box_class.h BoxContainer_class.h

SRCDIR=src
OBJDIR=obj
INCDIR=include

NAME=sdlapi.so

all: $(OBJDIR) $(NAME)

$(OBJDIR):
	mkdir -p $@

$(NAME): $(addprefix $(OBJDIR)/, $(OBJ))
	$(CC) $(CFLAGS) -shared -fPIC -ldl -o $@ $^ $(SDLLIB) $(SDLINC)

$(addprefix $(OBJDIR)/, %.o): $(addprefix $(SRCDIR)/, %.cpp) $(addprefix $(INCDIR)/, $(INC))
	$(CC) $(CFLAGS) -c -o $@ $< $(SDLINC)

clean:
	-/bin/rm -rf $(OBJDIR)

fclean:
	-/bin/rm -rf $(OBJDIR)
	-/bin/rm -f $(NAME)

re: fclean all
