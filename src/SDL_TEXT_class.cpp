/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SDL_TEXT_class.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/12 16:19:04 by bcherkas          #+#    #+#             */
/*   Updated: 2019/03/25 15:21:22 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SDL_TEXT_class.h"
#include "SDL_IMAGE_class.h"
#include <string>
#include <iostream>

SDL_TEXT	*SDL_TEXT::_ptr = nullptr;

SDL_TEXT	*SDL_TEXT::getAPI(SDL_API *apiptr) {
	if (SDL_TEXT::_ptr == nullptr)
		SDL_TEXT::_ptr = new SDL_TEXT(apiptr);
	return (SDL_TEXT::_ptr);
}

void		SDL_TEXT::destroyAPI() {
	delete SDL_TEXT::_ptr;
	SDL_TEXT::_ptr = nullptr;
}

SDL_TEXT::SDL_TEXT(SDL_API *apiptr) : _apiptr(apiptr), _allmap() {}

SDL_TEXT::~SDL_TEXT() {}

SDL_IMAGE	SDL_TEXT::renderText(std::string const fontpath, std::string const text, unsigned int color, int textsize) {
	TTF_Font	*font = fontManip(fontpath, textsize);

	if (!font)
		throw(std::runtime_error(TTF_GetError()));
	SDL_Color	rgb = {static_cast<unsigned char>((color >> 16) & 0xFF),
						static_cast<unsigned char>((color >> 8) & 0xFF),
						static_cast<unsigned char>(color & 0xFF),
						static_cast<unsigned char>((color >> 24) & 0xFF)};
	SDL_Surface	*surface = TTF_RenderUTF8_Blended(font, text.c_str(), rgb);
	if (!surface)
		throw(std::runtime_error(TTF_GetError()));
	return (SDL_IMAGE(_apiptr, surface));
}

TTF_Font	*SDL_TEXT::fontManip(std::string const &font, int ptsize) {
	auto	iter = _allmap.find(font);
	fontptr	ptr(nullptr, &TTF_CloseFont);

	if (iter != _allmap.end())
	{
		fontmap	&intmap = std::get<1>(*iter);
		auto	iter2 = intmap.find(ptsize);

		if (iter2 != intmap.end())
			return (std::get<1>(*iter2).get());
		TTF_Font	*f = TTF_OpenFont(font.c_str(), ptsize);
		if (f == NULL)
			return (nullptr);
		ptr.reset(f);
		intmap.insert(std::make_pair(ptsize, std::move(ptr)));
		return (f);
	}
	TTF_Font	*f = TTF_OpenFont(font.c_str(), ptsize);

	if (f == NULL)
		return (nullptr);
	ptr.reset(f);
	fontmap	intmap;
	
	intmap.insert(std::make_pair(ptsize, std::move(ptr)));
	_allmap.insert(std::make_pair(font, std::move(intmap)));
	return (f);
}
