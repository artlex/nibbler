/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ImageContainer_class.cpp                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/09 13:57:26 by bcherkas          #+#    #+#             */
/*   Updated: 2019/03/12 19:12:30 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ImageContainer_class.h"
#include "SDL_API_class.h"
#include "sdlapi.h"

ImageContainer	*ImageContainer::_ptr = nullptr;

ImageContainer	*ImageContainer::getContainer() {
	if (ImageContainer::_ptr == nullptr)
		ImageContainer::_ptr = new ImageContainer();
	return (ImageContainer::_ptr);
}

void			ImageContainer::destroyContainer() {
	delete ImageContainer::_ptr;
	ImageContainer::_ptr = nullptr;
}

ImageContainer::~ImageContainer() {}

/*
** Getters/Setters
*/

int				ImageContainer::addImage(SDL_IMAGE &&image) {
	SDL_IMAGE	*newimg = new SDL_IMAGE(std::move(image));
	imageptr	ptr(newimg);

	_imagemap.insert(std::make_pair(_imagemap_maxelement, ptr));
	return (_imagemap_maxelement++);
}

bool			ImageContainer::putImage(int index, int xpos, int ypos, int xsize, int ysize) {
	if (xsize < 0 || ysize < 0)
		return (false);
	auto	tmp = _imagemap.find(index);
	if (tmp == _imagemap.end())
		return (false);
	std::get<1>(*tmp)->putImage(xpos, ypos, xsize, ysize);
	return (true);
}

bool			ImageContainer::putImageRotate(int index, int xpos, int ypos, int xsize, int ysize, double angle) {
	if (xsize < 0 || ysize < 0)
		return (false);
	auto	tmp = _imagemap.find(index);
	if (tmp == _imagemap.end())
		return (false);
	std::get<1>(*tmp)->putImageRotate(xpos, ypos, xsize, ysize, angle);
	return (true);
}

ImageContainer::imageptr	ImageContainer::getImage(int index) {
	auto	iter = _imagemap.find(index);

	if (iter == _imagemap.end())
		return (imageptr(nullptr));
	return (std::get<1>(*iter));
}

bool			ImageContainer::delImage(int index) {
	auto	iter = _imagemap.find(index);

	if (iter == _imagemap.end())
		return (false);
	_imagemap.erase(iter);
	return (true);
}
