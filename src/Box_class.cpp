/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Box_class.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/17 16:21:13 by bcherkas          #+#    #+#             */
/*   Updated: 2019/03/26 19:29:08 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Box_class.h"
#include "AnimContainer_class.h"
#include "ImageContainer_class.h"

Box::Box() : posx(0), posy(0), width(0), height(0), _images(), _animations() {}

Box::Box(Box const &o) : posx(o.posx), posy(o.posy), width(o.width), height(o.height), _images(), _animations() {}

Box::Box(int x, int y, int w, int h) : posx(x), posy(y), width(w), height(h), _images(), _animations() {}

Box::~Box() {
	AnimContainer	*anims = AnimContainer::getContainer();

	for (auto j : _animations)
		anims->delAnimation(j);
}

Box		&Box::operator=(Box const &o) {
	posx = o.posx;
	posy = o.posy;
	width = o.width;
	height = o.width;
	return (*this);
}

bool	Box::intersect(int x, int y) const {
	if (x >= posx && x <= posx + width && y >= posy && y <= posy + height)
		return (true);
	return (false);
}

void	Box::addAnimation(int fd) const {
	_animations.insert(fd);
}
