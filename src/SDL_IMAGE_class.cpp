/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SDL_IMAGE_class.cpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 15:10:59 by bcherkas          #+#    #+#             */
/*   Updated: 2019/03/12 19:13:52 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SDL_IMAGE_class.h"
#include "SDL_API_class.h"
#include "SDL2/SDL_image.h"

SDL_IMAGE::SDL_IMAGE(SDL_API *apiptr, int width, int height) :
		_apiptr(apiptr),
		_surface(NULL, &SDL_FreeSurface),
		_texture(NULL, &SDL_DestroyTexture),
		_width(width), _height(height) {
	SDL_Texture		*texture = NULL;
	SDL_Surface		*surface = NULL;
    int				rmask = 0xff000000;
    int				gmask = 0x00ff0000;
    int				bmask = 0x0000ff00;
    int				amask = 0x000000ff;
	
	if (apiptr == NULL)
		throw(std::runtime_error("NULL pointer to SDL API"));
	surface = SDL_CreateRGBSurface(0, width, height, 32, rmask, gmask, bmask, amask);
	if (surface == NULL)
		throw(std::runtime_error(SDL_API::getError("Image surface creation failed")));
	_surface.reset(surface);
	texture = SDL_CreateTextureFromSurface(apiptr->getRenderer(), surface);
	if (texture == NULL)
		throw(std::runtime_error(SDL_API::getError("Image texture creation failed")));
	_texture.reset(texture);
}

SDL_IMAGE::SDL_IMAGE(SDL_API *apiptr, std::string const &str) :
		_apiptr(apiptr),
		_surface(NULL, &SDL_FreeSurface),
		_texture(NULL, &SDL_DestroyTexture),
		_width(0), _height(0) {
	SDL_Texture		*texture = NULL;
	SDL_Surface		*surface = NULL;

	if (apiptr == NULL)
		throw(std::runtime_error("NULL pointer to SDL API"));
	surface = IMG_Load(str.c_str());
	if (surface == NULL)
		throw(std::runtime_error(SDL_API::getImgError("Image surface creation failed")));
	_surface.reset(surface);
	_surface.reset(SDL_ConvertSurfaceFormat(surface, SDL_PIXELFORMAT_RGBA8888, 0));
	texture = SDL_CreateTextureFromSurface(apiptr->getRenderer(), _surface.get());
	if (texture == NULL)
		throw(std::runtime_error(SDL_API::getError("Image texture creation failed")));
	_texture.reset(texture);
	if (SDL_QueryTexture(texture, NULL, NULL, &_width, &_height) != 0)
		throw(std::runtime_error(SDL_API::getError("Image texture creation failed")));
}

SDL_IMAGE::SDL_IMAGE(SDL_API *apiptr, std::string const &str, unsigned int colorkey) :
		_apiptr(apiptr),
		_surface(NULL, &SDL_FreeSurface),
		_texture(NULL, &SDL_DestroyTexture),
		_width(0), _height(0) {
	SDL_Texture		*texture = NULL;
	SDL_Surface		*surface = NULL;
	unsigned int	rgb[3] = {(colorkey >> 16) & 0xFF, (colorkey >> 8) & 0xFF, colorkey & 0xFF};

	if (apiptr == NULL)
		throw(std::runtime_error("NULL pointer to SDL API"));
	surface = IMG_Load(str.c_str());
	if (surface == NULL)
		throw(std::runtime_error(SDL_API::getImgError("Image surface creation failed")));
	if (SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, rgb[0], rgb[1], rgb[2])) < 0)
		throw(std::runtime_error(SDL_API::getError("Image color keying creation failed")));
	_surface.reset(surface);
	_surface.reset(SDL_ConvertSurfaceFormat(surface, SDL_PIXELFORMAT_RGBA8888, 0));
	texture = SDL_CreateTextureFromSurface(apiptr->getRenderer(), _surface.get());
	if (texture == NULL)
		throw(std::runtime_error(SDL_API::getError("Image texture creation failed")));
	_texture.reset(texture);
	if (SDL_QueryTexture(texture, NULL, NULL, &_width, &_height) != 0)
		throw(std::runtime_error(SDL_API::getError("Image texture creation failed")));
}

SDL_IMAGE::SDL_IMAGE(SDL_API *apiptr, SDL_Surface *surface) : _apiptr(apiptr),
			_surface(surface, &SDL_FreeSurface),
			_texture(nullptr, &SDL_DestroyTexture),
			_width(0), _height(0) {
	SDL_Texture		*texture = nullptr;

	if (!apiptr)
		throw(std::runtime_error("NULL pointer to SDL API"));
	if (!surface)
		throw(std::runtime_error("NULL pointer to surface"));
	_surface.reset(SDL_ConvertSurfaceFormat(surface, SDL_PIXELFORMAT_RGBA8888, 0));
	texture = SDL_CreateTextureFromSurface(apiptr->getRenderer(), _surface.get());
	if (texture == NULL)
		throw(std::runtime_error(SDL_API::getError("Image texture creation failed")));
	_texture.reset(texture);
	if (SDL_QueryTexture(texture, NULL, NULL, &_width, &_height) != 0)
		throw(std::runtime_error(SDL_API::getError("Image texture creation failed")));
}

SDL_IMAGE::SDL_IMAGE(SDL_IMAGE &&img) : _apiptr(img._apiptr),
										_surface(img._surface.release(), &SDL_FreeSurface),
										_texture(img._texture.release(), &SDL_DestroyTexture),
										_width(img._width),
										_height(img._height) {
	img._apiptr = nullptr;
}

SDL_IMAGE::~SDL_IMAGE() {}

bool		SDL_IMAGE::fillImage(unsigned int r, unsigned int g, unsigned int b) {
	SDL_Texture		*texture = NULL;

	if (SDL_FillRect(_surface.get(), NULL, SDL_MapRGB(_surface->format, r, g, b)) < 0)
		return (false);
	texture = SDL_CreateTextureFromSurface(_apiptr->getRenderer(), _surface.get());
	if (texture == NULL)
		return (false);
	_texture.reset(texture);
	return (true);
}

bool		SDL_IMAGE::putImage(int x, int y, int sizex, int sizey) {
	if (sizex < 0 || sizey < 0)
		return (false);
	SDL_Rect	dstrect = {x, y,
		(sizex == 0) ? _width : sizex, (sizey == 0) ? _height : sizey};

	if (!SDL_RenderCopy(_apiptr->getRenderer(), _texture.get(), NULL, &dstrect))
		return (true);
	return (false);
}

bool		SDL_IMAGE::putImageRotate(int x, int y, int sizex, int sizey, double angle) {
	if (sizex < 0 || sizey < 0)
		return (false);
	SDL_Rect			dstrect = {x, y,
		(sizex == 0) ? _width : sizex, (sizey == 0) ? _height : sizey};
	SDL_RendererFlip	flip = static_cast<SDL_RendererFlip>(SDL_FLIP_HORIZONTAL | SDL_FLIP_VERTICAL);

	if (!SDL_RenderCopyEx(_apiptr->getRenderer(), _texture.get(), NULL, &dstrect, angle, NULL, flip))
		return (true);
	return (false);
}

/*
** Getters/Setter
*/

SDL_Surface	*SDL_IMAGE::getSurface() {
	return (_surface.get());
}

SDL_Texture	*SDL_IMAGE::getTexture() {
	return (_texture.get());
}

int				SDL_IMAGE::getWidth() const {
	return (_width);
}

int				SDL_IMAGE::getHeight() const {
	return (_height);
}

void			SDL_IMAGE::getSize(int &width, int &height) const {
	width = _width;
	height = _height;
}
