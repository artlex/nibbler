/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   BoxContainer.cpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/17 16:31:04 by bcherkas          #+#    #+#             */
/*   Updated: 2019/03/17 18:08:22 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "BoxContainer_class.h"
#include <algorithm>

BoxContainer	*BoxContainer::_ptr = nullptr;

BoxContainer	*BoxContainer::getContainer() {
	if (BoxContainer::_ptr == nullptr)
		BoxContainer::_ptr = new BoxContainer();
	return (BoxContainer::_ptr);
}

void			BoxContainer::destroyContainer() {
	delete BoxContainer::_ptr;
	BoxContainer::_ptr = nullptr;
}

BoxContainer::~BoxContainer() {}

Box const		*BoxContainer::createBox(int x, int y, int width, int height) {
	if (width < 1 || height < 1)
		return (nullptr);
	_list.emplace_front(x, y, width, height);
	return (&_list.front());
}

Box const		*BoxContainer::findFirstIntersection(int x, int y) {
	auto	func = [&x, &y](Box const &box) {return box.intersect(x, y);};
	auto	iter = std::find_if(_list.begin(), _list.end(), func);

	if (iter == _list.end())
		return (nullptr);
	return (&(*iter));
}

bool			BoxContainer::exist(Box const *box) {
	if (!box)
		return (false);
	auto	func = [box](Box const &boxref) {
		if (&boxref == box)
			return (true);
		return (false);
	};
	auto	iter = std::find_if(_list.begin(), _list.end(), func);

	if (iter == _list.end())
		return (false);
	return (true);
}

bool			BoxContainer::deleteBox(Box const *box) {
	if (!box)
		return (false);
	auto	func = [box](Box const &boxref) {
		if (&boxref == box)
			return (true);
		return (false);
	};
	auto	iter = std::find_if(_list.begin(), _list.end(), func);

	if (iter == _list.end())
		return (false);
	_list.erase(iter);
	return (true);
}
