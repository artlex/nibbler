/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AnimContainer_class.cpp                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/09 13:57:26 by bcherkas          #+#    #+#             */
/*   Updated: 2019/03/26 18:48:45 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AnimContainer_class.h"
#include "sdlapi.h"
#include "SDL_API_class.h"

AnimContainer	*AnimContainer::_ptr = nullptr;

AnimContainer	*AnimContainer::getContainer() {
	if (AnimContainer::_ptr == nullptr)
		AnimContainer::_ptr = new AnimContainer();
	return (AnimContainer::_ptr);
}

void			AnimContainer::destroyContainer() {
	delete AnimContainer::_ptr;
	AnimContainer::_ptr = nullptr;
}

AnimContainer::~AnimContainer() {}

AnimContainer::AnimPos::AnimPos(int xpos, int ypos, int xsize, int ysize, SDL_ANIM const &anim) :
	_xpos(xpos), _ypos(ypos), _xsize(xsize), _ysize(ysize),
	_anim(anim), _angle(180), _pause(false), _hide(false) {}

AnimContainer::AnimPos::AnimPos(AnimContainer::AnimPos const &o) :
	_xpos(o._xpos), _ypos(o._ypos), _xsize(o._xsize), _ysize(o._ysize),
	_anim(o._anim), _angle(o._angle), _pause(o._pause), _hide(o._hide) {}

AnimContainer::AnimPos::~AnimPos() {}

/*
** Getters/Setters
*/

int				AnimContainer::addAnimation(SDL_ANIM &image) {
	_imagemap.insert(std::make_pair(_imagemap_maxelement, std::move(image)));
	return (_imagemap_maxelement++);
}

int				AnimContainer::addAnimation(SDL_ANIM &&image) {
	_imagemap.insert(std::make_pair(_imagemap_maxelement, std::move(image)));
	return (_imagemap_maxelement++);
}

int				AnimContainer::putAnimation(int index, int x, int y, int xsize, int ysize) {
	if (xsize < 0 || ysize < 0)
		return (-1);
	if (_imagemap.find(index) == _imagemap.end())
		return (-1);
	AnimPos		pos(x, y, xsize, ysize, _imagemap.at(index));
	_activeAnimations.insert(std::make_pair(_activemap_maxelement, pos));
	return (_activemap_maxelement++);
}

bool			AnimContainer::moveActive(int index, int x, int y) {
	auto	iter = _activeAnimations.find(index);

	if (iter == _activeAnimations.end())
		return (false);
	std::get<1>(*iter)._xpos = x;
	std::get<1>(*iter)._ypos = y;
	return (true);
}

bool			AnimContainer::rotateActive(int index, double angle) {
	auto	iter = _activeAnimations.find(index);

	if (iter == _activeAnimations.end())
		return (false);
	std::get<1>(*iter)._angle = angle;
	return (true);
}

void			AnimContainer::drawActive()
{
	if (_activeAnimations.size() < 1)
		return ;
	auto	func = [](AnimPos &obj) {
		if (!obj._pause && !obj._anim.processAnimation())
			return (true);
		if (!obj._hide)
			obj._anim.putAnimation(obj._xpos, obj._ypos, obj._xsize, obj._ysize, obj._angle);
		return (false);
	};

	for (auto i = _activeAnimations.begin(); i != _activeAnimations.end(); ) {
		AnimPos		&tmp = std::get<1>(*i);
		if (func(tmp))
			i = _activeAnimations.erase(i);
		else
			++i;
	}
}

bool			AnimContainer::pauseActive(int index, bool pause) {
	auto	iter = _activeAnimations.find(index);

	if (iter == _activeAnimations.end())
		return (false);
	AnimPos	&posref = std::get<1>(*iter);
	posref._pause = pause;
	return (true);
}

bool			AnimContainer::hideActive(int index, bool hide) {
	auto	iter = _activeAnimations.find(index);

	if (iter == _activeAnimations.end())
		return (false);
	AnimPos	&posref = std::get<1>(*iter);
	posref._hide = hide;
	return (true);
}

bool			AnimContainer::shiftActive(int index, unsigned int ticks) {
	auto	iter = _activeAnimations.find(index);

	if (iter == _activeAnimations.end())
		return (false);
	AnimPos	&posref = std::get<1>(*iter);
	posref._anim.changeAnimationTick(ticks);
	return (true);
}

bool			AnimContainer::delAnimation(int index) {
	auto	iter = _imagemap.find(index);

	if (iter == _imagemap.end())
		return (false);
	_imagemap.erase(iter);
	return (true);
}

bool			AnimContainer::delActive(int index) {
	auto	iter = _activeAnimations.find(index);

	if (iter == _activeAnimations.end())
		return (false);
	_activeAnimations.erase(iter);
	return (true);
}
