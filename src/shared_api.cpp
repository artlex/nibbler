/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shared_api.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 19:57:27 by bcherkas          #+#    #+#             */
/*   Updated: 2019/03/26 19:28:18 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sdlapi.h"
#include <string>
#include "SDL_API_class.h"
#include "SDL_IMAGE_class.h"
#include "SDL_ANIM_class.h"
#include "ImageContainer_class.h"
#include "AnimContainer_class.h"
#include "SDL_TEXT_class.h"
#include "Box_class.h"
#include "BoxContainer_class.h"

/*
** Initialization and deinit
*/

extern "C" bool		init_api(int width, int height)
{
	try {
		SDL_API			*api = SDL_API::getAPI(width, height);

		api->createRenderer(SDL_WINDOW_OPENGL, SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE);
	}
	catch (std::exception &e)
	{
		std::cout << e.what() << std::endl;
		return (false);
	};
	return true;
}

extern "C" void		quit_api()
{
	BoxContainer::destroyContainer();
	ImageContainer::destroyContainer();
	AnimContainer::destroyContainer();
	SDL_TEXT::destroyAPI();
	SDL_API::destroyAPI();
}

/*
** Screen and events manip
*/

extern "C" bool		checkEvents()
{
	return (SDL_API::getAPI()->checkEvents());
}

extern "C" void		update()
{
	return (SDL_API::getAPI()->update());
}

extern "C" void		setMoveFunc(void (*f)(int))
{
	SDL_API::getAPI()->setMoveHandler(f);
}


extern "C" bool		clear_screen()
{
	if (!SDL_API::getAPI()->clearScreen())
		return (true);
	return (false);
}

/*
** Images
*/ 

extern "C" int		createText(const char *fontpath, const char *text, unsigned int color, int fontsize)
{
	if (!fontpath || !text)
			return (-1);
	SDL_API			*api = SDL_API::getAPI();
	ImageContainer	*cont = ImageContainer::getContainer();

	try
	{
		return (cont->addImage(SDL_TEXT::getAPI(api)->renderText(fontpath, text, color, fontsize)));
	}
	catch (std::exception &e) {
		std::cout << e.what() << std::endl;
		return (-1);
	};
}

extern "C" int		createImage(char const *path)
{
	if (!path)
		return (-1);
	try {
		SDL_API			*api = SDL_API::getAPI();
		ImageContainer	*cont = ImageContainer::getContainer();
		return (cont->addImage(SDL_IMAGE(api, path)));
	}
	catch (std::exception &e) {
		std::cout << e.what() << std::endl;
		return (-1);
	};
}

extern "C" int		createImageTransparent(char const *path, unsigned int color)
{
	if (!path)
		return (-1);
	try {
		SDL_API			*api = SDL_API::getAPI();
		ImageContainer	*cont = ImageContainer::getContainer();

		return (cont->addImage(SDL_IMAGE(api, path, color)));
	}
	catch (std::exception &e) {
		std::cout << e.what() << std::endl;
		return (-1);
	};
}

extern "C" bool		putimage(int index, int xpos, int ypos, int xsize, int ysize)
{
	return (ImageContainer::getContainer()->putImage(index, xpos, ypos, xsize, ysize));
}

extern "C" bool		putimagerotate(int index, int xpos, int ypos, int xsize, int ysize, double angle)
{
	return (ImageContainer::getContainer()->putImageRotate(index, xpos, ypos, xsize, ysize, angle));
}

/*
** Animations
*/

extern "C" int		createAnimation(int *integers, unsigned int cycle_step, unsigned int max_cycles)
{
	ImageContainer	*imgcont = ImageContainer::getContainer();
	AnimContainer	*animcont = AnimContainer::getContainer();
	std::vector<ImageContainer::imageptr>	vect;

	if (!integers)
		return (-1);
	for (int i = 0; integers[i] >= 0; ++i) {
		auto img = imgcont->getImage(integers[i]);

		if (img.get() != nullptr)
			vect.push_back(imgcont->getImage(integers[i]));
	}
	if (vect.size() < 1)
		return (-1);
	return (animcont->addAnimation(SDL_ANIM(std::move(vect), cycle_step, max_cycles)));
}

extern "C" int		putanimation(int index, int xpos, int ypos, int xsize, int ysize)
{
	return (AnimContainer::getContainer()->putAnimation(index, xpos, ypos, xsize, ysize));
}

extern "C" bool		moveAnimation(int index, int xpos, int ypos)
{
	return (AnimContainer::getContainer()->moveActive(index, xpos, ypos));
}

extern "C" bool		rotateAnimation(int index, double angle)
{
	return (AnimContainer::getContainer()->rotateActive(index, angle));
}

extern "C" bool		pauseAnimation(int index, bool pause)
{
	return (AnimContainer::getContainer()->pauseActive(index, pause));
}

extern "C" bool		hideAnimation(int index, bool hide)
{
	return (AnimContainer::getContainer()->hideActive(index, hide));
}

extern "C" bool		shiftAnimation(int index, unsigned int ticks)
{
	return (AnimContainer::getContainer()->shiftActive(index, ticks));
}

/*
** Deletion of images and animations
*/

extern "C" bool		delimage(int index)
{
	return (ImageContainer::getContainer()->delImage(index));
}

extern "C" bool		delanimation(int index)
{
	return (AnimContainer::getContainer()->delAnimation(index));
}

extern "C" bool		delactive(int index)
{
	return (AnimContainer::getContainer()->delActive(index));
}

/*
** Box
*/

extern "C" Box const	*createbox(int x, int y, int width, int height)
{
	Box const	*ret = BoxContainer::getContainer()->createBox(x, y, width, height);

	if (!ret)
		return (NULL);
	return (ret);
}

extern "C" Box const	*getmousestate(int *pressed)
{
	int		x;
	int		y;

	if (SDL_API::getAPI()->getMouseState(&x, &y))
	{
		if (pressed != NULL)
			*pressed = 1;
	}
	else
		if (pressed != NULL)
			*pressed = 0;
	return (BoxContainer::getContainer()->findFirstIntersection(x, y));
}

extern "C" bool			fillboximage(int image_fd, Box const *box)
{
	if (!BoxContainer::getContainer()->exist(box))
		return (false);
	return (ImageContainer::getContainer()->putImage(image_fd,
				box->posx, box->posy, box->width, box->height));
}

extern "C" int			fillboxanimation(int animation_fd, Box const *box)
{
	int		fd;
	if (!BoxContainer::getContainer()->exist(box))
		return (-1);
	fd = AnimContainer::getContainer()->putAnimation(animation_fd,
				box->posx, box->posy, box->width, box->height);
	if (fd != -1)
		box->addAnimation(fd);
	return (fd);
}

extern "C" bool			deletebox(Box const *box)
{
	return (BoxContainer::getContainer()->deleteBox(box));
}

/*
** System API funcs
*/

extern "C" void		printError(const char *str)
{
	if (str)
		std::cerr << str << " : " << SDL_GetError() << std::endl;
	else
		std::cerr << SDL_GetError() << std::endl;
}

extern "C" unsigned int	getTime() {
	return (SDL_API::getAPI()->getTime());
}

extern "C" bool		getMousePos(int *x, int *y)
{
	return (SDL_API::getAPI()->getMouseState(x, y));
}
