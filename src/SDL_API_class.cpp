/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SDL_API_class.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/06 17:48:41 by bcherkas          #+#    #+#             */
/*   Updated: 2019/05/02 17:17:40 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sdlapi.h"
#include "SDL_API_class.h"
#include "SDL2/SDL_image.h"
#include "SDL2/SDL_ttf.h"
#include "AnimContainer_class.h"
#include <sys/time.h>

void		SDL_API::buttonEvent(SDL_Event &event)
{
	std::map<unsigned int, int>	keymap{
		{SDL_SCANCODE_LEFT, LEFT},
		{SDL_SCANCODE_UP, UP},
		{SDL_SCANCODE_RIGHT, RIGHT},
		{SDL_SCANCODE_DOWN, DOWN},
		{SDL_SCANCODE_1, ONE},
		{SDL_SCANCODE_2, TWO},
		{SDL_SCANCODE_3, THREE},
		{SDL_SCANCODE_4, FOUR},
		{SDL_SCANCODE_5, FIVE},
		{SDL_SCANCODE_6, SIX},
		{SDL_SCANCODE_7, SEVEN},
		{SDL_SCANCODE_8, EIGHT},
		{SDL_SCANCODE_9, NINE},
		{SDL_SCANCODE_0, ZERO}
	};
	auto	iter = keymap.find(event.key.keysym.scancode);
	if (iter == keymap.end())
		return ;
	_movehandler(std::get<1>(*iter));
}

SDL_API		*SDL_API::_ptr = nullptr;

SDL_API		*SDL_API::getAPI(int width, int height)
{
	if (SDL_API::_ptr == nullptr)
		SDL_API::_ptr = new SDL_API(width, height);
	return (SDL_API::_ptr);
}

void		SDL_API::destroyAPI() {
	delete SDL_API::_ptr;
	SDL_API::_ptr = nullptr;
}

std::string	SDL_API::getImgError(std::string const &str)
{
	return (str + ", SDL error: " + IMG_GetError());
}

std::string	SDL_API::getError(std::string const &str)
{
	return (str + ", SDL error: " + SDL_GetError());
}

void		SDL_API::printError(char const *str)
{
	if (str)
		std::cout << str << ", SDL erro: " << SDL_GetError() << std::endl;
	else
		std::cout << "SDL error: " << SDL_GetError() << std::endl;
}

SDL_API::SDL_API(size_t width, size_t height) : _width(width), _height(height),
		_window(nullptr, &SDL_DestroyWindow),
		_renderer(nullptr, &SDL_DestroyRenderer) {
	int flags = IMG_INIT_JPG | IMG_INIT_PNG;
	int	initted;

	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_EVENTS) < 0)
		throw(std::runtime_error(SDL_API::getError("SDL init failed")));
	initted = IMG_Init(flags);
	if ((initted & flags) != flags)
		throw(std::runtime_error(SDL_API::getImgError("SDL image init failed")));
	if (TTF_Init() == -1)
		throw(std::runtime_error(SDL_API::getImgError("SDL ttf init failed")));
}

SDL_API::~SDL_API() {
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

bool		SDL_API::createRenderer(int winflags, int rendflags)
{
	int const		winpos = SDL_WINDOWPOS_CENTERED;
	SDL_Window		*window;
	SDL_Renderer	*renderer;
	
	if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
		printf( "Warning: Linear texture filtering not enabled!");
	window = SDL_CreateWindow("Nibbler SDL_API",
			winpos, winpos, _width, _height, winflags);
	if (window == NULL)
		return (false);
	else
		_window.reset(window);
	renderer = SDL_CreateRenderer(window, -1, rendflags);
	if (renderer == NULL)
		return (false);
	else
		_renderer.reset(renderer);
	SDL_SetRenderDrawColor(_renderer.get(), 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(_renderer.get());
	return (true);
}

bool		SDL_API::checkEvents() {
	SDL_Event	event;

	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT)
			return (false);
		else if (event.type == SDL_KEYDOWN &&
				event.key.keysym.scancode == SDL_SCANCODE_ESCAPE)
			return (false);
		if (_hasMoveHandler && _movehandler && event.type == SDL_KEYDOWN)
		{
			SDL_Event	event2;

			event2 = event;
			while (SDL_PollEvent(&event))
				;
			buttonEvent(event2);
			break ;
		}
	}
	return (true);
}

void		SDL_API::update() {
	AnimContainer::getContainer()->drawActive();
	SDL_RenderPresent(_renderer.get());
}

bool		SDL_API::clearScreen() {
	if (!SDL_RenderClear(_renderer.get()))
		return (true);
	return (false);
}

/*
** Getters/Setters
*/

SDL_Renderer	*SDL_API::getRenderer() {
	return (_renderer.get());
}

SDL_Window		*SDL_API::getWindow() {
	return (_window.get());
}

int				SDL_API::getWindowWidth() const {
	return (_width);
}

int				SDL_API::getWindowHeight() const {
	return (_height);
}

void			SDL_API::getWindowSize(int &width, int &height) const {
	width = _width;
	height = _height;
}

bool			SDL_API::getMouseState(int *x, int *y) const {
	return (SDL_GetMouseState(x, y) == SDL_BUTTON(SDL_BUTTON_LEFT));
}

unsigned int	SDL_API::getTime() const {
	return (SDL_GetTicks());
}

void			SDL_API::setMoveHandler(void (*f)(int)) {
	if (f == nullptr)
		_hasMoveHandler = false;
	else
	{
		_movehandler = f;
		_hasMoveHandler = true;
	}
}
