/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SDL_IMAGE_class.cpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 15:10:59 by bcherkas          #+#    #+#             */
/*   Updated: 2019/03/26 15:20:32 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SDL_IMAGE_class.h"
#include "SDL_ANIM_class.h"
#include <iostream>

SDL_ANIM::SDL_ANIM(imagevector &&images, unsigned int cycles_step, unsigned int cycles) :
	_imgvector(images), _cycles_step(cycles_step), _lifetime_cycles(cycles),
	_size(_imgvector.size()) {}

SDL_ANIM::SDL_ANIM(SDL_ANIM const &obj) :
	_imgvector(obj._imgvector), _cycles_step(obj._cycles_step),
	_lifetime_cycles(obj._lifetime_cycles), _size(obj._size) {}

SDL_ANIM::SDL_ANIM(SDL_ANIM &&obj) :
	_imgvector(std::move(obj._imgvector)), _cycles_step(obj._cycles_step),
	_lifetime_cycles(obj._lifetime_cycles), _size(obj._size) {}

SDL_ANIM::~SDL_ANIM() {}

void		SDL_ANIM::changeAnimationTick(unsigned int ticks) {
	_iter = (_iter + ticks) % _size;
}

bool		SDL_ANIM::processAnimation(void) {
	_cycles++;
	if (_lifetime_cycles > 0 && _lifetime_cycles < _cycles)
		return (false);
	if (_cycles_step != 0 && _cycles % _cycles_step == 0)
		_iter = (_iter + 1) % _size;
	return (true);
}

bool		SDL_ANIM::putAnimation(int x, int y, int sizex, int sizey, double angle) {

	if (sizex < 1 || sizey < 1)
		return (false);
	_imgvector[_iter]->putImageRotate(x, y, sizex, sizey, angle);
	return (true);
}
